FROM golang:1.12 AS makisu-builder
RUN CGO_ENABLED=0 GOOS=linux go get -v github.com/uber/makisu/bin/makisu

FROM golang:1.11 AS gcr_cred_helper_builder
RUN go get -u github.com/GoogleCloudPlatform/docker-credential-gcr
RUN CGO_ENABLED=0 make -C /go/src/github.com/GoogleCloudPlatform/docker-credential-gcr && \
    cp /go/src/github.com/GoogleCloudPlatform/docker-credential-gcr/bin/docker-credential-gcr /docker-credential-gcr


FROM golang:1.11 AS ecr_cred_helper_builder
RUN go get -u github.com/awslabs/amazon-ecr-credential-helper/ecr-login/cli/docker-credential-ecr-login
RUN make -C /go/src/github.com/awslabs/amazon-ecr-credential-helper linux-amd64

FROM bash
COPY --from=makisu-builder /go/bin/makisu /makisu-internal/makisu
COPY --from=makisu-builder /go/src/github.com/uber/makisu/assets/cacerts.pem /makisu-internal/certs/cacerts.pem
COPY --from=gcr_cred_helper_builder /docker-credential-gcr /makisu-internal/docker-credential-gcr
COPY --from=ecr_cred_helper_builder \
  /go/src/github.com/awslabs/amazon-ecr-credential-helper/bin/linux-amd64/docker-credential-ecr-login \
  /makisu-internal/docker-credential-ecr-login
